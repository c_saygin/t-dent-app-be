const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const usersRoutes = require('./routes/users');
const appointmentRoutes = require('./routes/appointments');

const app = express();

// kuvVZk3sMhhet1GM

// mongoose.connect('mongodb://can_saygin:kuvVZk3sMhhet1GM@cluster0-shard-00-00-msc2r.mongodb.net:27017,cluster0-shard-00-01-msc2r.mongodb.net:27017,cluster0-shard-00-02-msc2r.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true })
//     .then(() => {
//         console.log('Connected to MongoDB');
//     })
//     .catch((e) => {
//         console.log('Connection to MongoDB failed', e);
//     });

mongoose
    .connect(
        'mongodb://localhost:27017/t-dent-app', { useNewUrlParser: true }
    )
    .then(() => console.log('Connected to MongoDB...'))
    .catch(() => console.log('Failed to connect MongoDB'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    );
    next();
});

app.use('/api/users', usersRoutes);
app.use('/api/appointments', appointmentRoutes);

module.exports = app;