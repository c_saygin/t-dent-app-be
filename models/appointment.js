const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const { userSchema } = require('./user');

const appointmentSchema = mongoose.Schema({
    startDate: { type: Date, required: true },
    endDate: { type: Date, required: true },
    patient: {
        type: new mongoose.Schema({
            name: {
                type: String,
                required: true,
                trim: true
            },
            phoneNumber: {
                type: String,
                required: true
            }
        }),
        required: true
    },
    doctor: {
        type: String
    },
    notes: {
        type: String,
        trim: true
    },
    createdByUserId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }
});

appointmentSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Appointment', appointmentSchema);