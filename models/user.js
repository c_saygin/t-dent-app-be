const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Joi = require('joi');
const moment = require('moment');

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true,
        // unique: true
    },
    adress: {
        district: {
            type: String,
            required: true
        },
        street: {
            type: String,
            required: true
        },
        state: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        apartmentNo: {
            type: Number,
            required: true
        },
        flatNo: {
            type: Number,
            required: true
        }
    },
    email: {
        type: String,
        required: false,
    },
    password: {
        type: String,
        required: true
    },
    birthday: {
        type: Date,
        required: true
    },
    userType: {
        type: Number,
        required: true
    }
});

const User = mongoose.model('User', userSchema);

function validateUser() {
    const schema = {
        name: Joi.string().min(2).max(50).required(),
        phoneNumber: Joi.string().min(7).max(50).required(),
        adress: Joi.string().min(7).max(50).required(),
        email: Joi.string().min(5).max(50).required(),
        password: Joi.string().min(3).max(50).required()
    }
}

userSchema.plugin(uniqueValidator);

exports.User = User;
exports.userSchema = userSchema;
exports.validate = validateUser;