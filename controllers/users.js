const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/user');

const mailService = require('../utils/mail-service');

exports.createUser = (req, res, next) => {
	bcrypt.hash(req.body.password, 10).then(hash => {
		const user = new User({
			name: req.body.name,
			phoneNumber: req.body.phoneNumber,
			adress: req.body.adress,
			email: req.body.email,
			password: hash,
			birthday: req.body.birthday,
			userType: req.body.userType
		});
		user.save()
			.then(result => {
				mailService.sendMail(
					result.email,
					req.body.name,
					req.body.password
				);
				res.status(201).json({
					message: 'User created',
					user: result
				});
			})
			.catch(err => {
				res.status(500).json({
					message: 'A server error occurred while creating user'
				});
			});
	});
};

exports.loginUser = (req, res, next) => {
	let fetchedUser;
	User.findOne({ phoneNumber: req.body.phoneNumber })
		.then(user => {
			if (!user) {
				return res.status(404).json({
					message: 'Entered password or phone number is wrong'
				});
			}
			fetchedUser = user;
			return bcrypt.compare(req.body.password, user.password);
		})
		.then(result => {
			if (!result) {
				return res.status(401).json({
					message: 'Auth failed'
				});
			}

			const token = jwt.sign({
				phoneNumber: fetchedUser.phoneNumber,
				userId: fetchedUser._id,
				userName: fetchedUser.name,
				userType: fetchedUser.userType
			},
				'secret_private_key_make_it_env_variable' // TODO: this should be an environment variable
			);

			res.status(200).json({
				token: token,
				userId: fetchedUser._id
			});
		})
		.catch(err => {
			return res.status(500).json({
				message: 'A server error occurred while user login operation'
			});
		});
};

// Patient Operations

exports.getPatients = (req, res, next) => {
	const patientQuery = User.find({ userType: 3 }).select('-password');
	let fetchedPatients;

	patientQuery
		.then(patients => {
			fetchedPatients = patients;
			return User.count({ userType: 3 });
		})
		.then(count => {
			res.status(200).json({
				message: 'Patients fetched successfully!',
				patients: fetchedPatients,
				maxPatients: count
			});
		})
		.catch(error => {
			res.status(500).json({
				message: 'Fetching patients failed ' + error
			});
		});
};

exports.updatePatient = (req, res, next) => {
	if (req.body.userType !== 3) {
		res.status(400).json({
			message: 'User to update is not a patient...'
		});
		return;
	}

	const user = new User({
		_id: req.body.id,
		name: req.body.name,
		phoneNumber: req.body.phoneNumber,
		adress: req.body.adress,
		email: req.body.email,
		password: req.body.password,
		birthday: req.body.birthday,
		userType: req.body.userType
	});
	User.updateOne({ _id: req.params.id }, user)
		.then(result => {
			if (result.n > 0) {
				res.status(200).json({ message: 'Update successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Could not update the user with id: ' + _id
			});
		});
};

exports.getPatientByPhoneNumber = (req, res, next) => {
	const patientQuery = User.findOne({ phoneNumber: req.params.phoneNumber })
		.where({ userType: 3 })
		.select('-password');

	patientQuery
		.then(document => {
			res.status(200).json({
				message: 'Patient fetched successfully',
				patient: document
			});
		})
		.catch(error => {
			res.status(500).json({
				message: 'A server error occurred while fetching patient by phone number'
			});
		});
};

exports.getPatientById = (req, res, next) => {
	User.findOne({ _id: req.params.id }).select('-password')
		.then(user => {
			res.status(200).json({
				message: 'Patient fetched successfully',
				patient: user
			});
		})
		.catch(error => {
			res.status(500).json({
				message: 'A server error occurred while retrieving patient with id ' + req.params.id + ' :' + error
			});
		});
};

exports.deletePatient = (req, res, next) => {
	User.deleteOne({ _id: req.params.id })
		.then(result => {
			console.log(result);
			if (result.n > 0) {
				res.status(200).json({ message: 'Deletion successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Deleting the user with id: ' + req.params.id + ' failed...'
			});
		});
};

// Nurse Operations

exports.getNurses = (req, res, next) => {
	const nurseQuery = User.find({ userType: 2 }).select('-password');
	let fetchedNurses;

	nurseQuery
		.then(nurses => {
			fetchedNurses = nurses;
			res.status(200).json({
				message: 'Nurses fetched successfully',
				nurses: fetchedNurses
			})
		})
		.catch(error => {
			res.status(500).json({
				message: 'Fetching nurses failed ' + error
			});
		});
};

exports.updateNurse = (req, res, next) => {
	if (req.body.userType !== 2) {
		res.status(400).json({
			message: 'User to update is not a nurse...'
		});
		return;
	}

	const user = new User({
		_id: req.body.id,
		name: req.body.name,
		phoneNumber: req.body.phoneNumber,
		adress: req.body.adress,
		email: req.body.email,
		password: req.body.password,
		birthday: req.body.birthday,
		userType: req.body.userType
	});
	User.updateOne({ _id: req.params.id }, user)
		.then(result => {
			if (result.n > 0) {
				res.status(200).json({ message: 'Update successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Could not update the user with id ' + _id + ': ' + error
			});
		});
};

exports.getNurseByPhoneNumber = (req, res, next) => {
	const nurseQuery = User.findOne({ phoneNumber: req.params.phoneNumber })
		.where({ userType: 2 })
		.select('-password');

	nurseQuery
		.then(document => {
			res.status(200).json({
				message: 'Nurse fetched successfully',
				nurse: document
			});
		})
		.catch(error => {
			res.status(500).json({
				message: 'A server error occurred while fetching patient by phone number: ' + error
			});
		});
};

exports.deleteNurse = (req, res, next) => {
	User.deleteOne({ _id: req.params.id })
		.then(result => {
			console.log(result);
			if (result.n > 0) {
				res.status(200).json({ message: 'Deletion successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Deleting the user with id: ' + req.params.id + ' failed: ' + error
			});
		});
};

// Doctor Operations

exports.getDoctors = (req, res, next) => {
	const doctorQuery = User.find({ userType: 1 }).select('-password');
	let fetchedDoctors;

	doctorQuery
		.then(doctors => {
			fetchedDoctors = doctors;
			res.status(200).json({
				message: 'Doctors fetched successfully',
				doctors: fetchedDoctors
			})
		})
		.catch(error => {
			res.status(500).json({
				message: 'Fetching doctors failed ' + error
			});
		});
};

exports.updateDoctor = (req, res, next) => {
	if (req.body.userType !== 1) {
		res.status(400).json({
			message: 'User to update is not a doctor...'
		});
		return;
	}

	const user = new User({
		_id: req.body.id,
		name: req.body.name,
		phoneNumber: req.body.phoneNumber,
		adress: req.body.adress,
		email: req.body.email,
		password: req.body.password,
		birthday: req.body.birthday,
		userType: req.body.userType
	});
	User.updateOne({ _id: req.params.id }, user)
		.then(result => {
			if (result.n > 0) {
				res.status(200).json({ message: 'Update successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Could not update the doctor with id ' + _id + ': ' + error
			});
		});
};

exports.getDoctorByPhoneNumber = (req, res, next) => {
	const doctorQuery = User.findOne({ phoneNumber: req.params.phoneNumber })
		.where({ userType: 1 })
		.select('-password');

	doctorQuery
		.then(document => {
			res.status(200).json({
				message: 'Doctor fetched successfully',
				doctor: document
			});
		})
		.catch(error => {
			res.status(500).json({
				message: 'A server error occurred while fetching doctor by phone number: ' + error
			});
		});
};

exports.deleteDoctor = (req, res, next) => {
	User.deleteOne({ _id: req.params.id })
		.then(result => {
			console.log(result);
			if (result.n > 0) {
				res.status(200).json({ message: 'Deletion successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized' });
			}
		})
		.catch(error => {
			res.status(500).json({
				message: 'Deleting the doctor with id: ' + req.params.id + ' failed: ' + error
			});
		});
};