const User = require('../models/user');

// exports.getAllPatients = async(req, res, next) => {
//     const patients = await User.find({ userType: 3 }).select('-password');
//     res.send(patients);
// }

exports.getAllPatients = (req, res, next) => {
    const pageSize = +req.query.pagesize; // + needed to convert it to int from string
    const currentPage = +req.query.page;
    const patientQuery = User.find({ userType: 3 }).select('-password');
    let fetchedPatients;

    // if (pageSize && currentPage) {
    // 	patientQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
    // }

    patientQuery
        .then(patients => {
            fetchedPatients = patients;
            return User.count({ userType: 3 });
        })
        .then(count => {
            res.status(200).json({
                message: 'Patients fetched successfully!',
                patients: fetchedPatients,
                maxPatients: count
            });
        })
        .catch(error => {
            res.status(500).json({
                message: 'Fetching patients failed ' + error
            });
        });
};