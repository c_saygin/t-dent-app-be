const Appointment = require('../models/appointment');
const { User } = require('../models/user');
var moment = require('moment');

exports.getAppointmentsByDate = (req, res, next) => {
    const milliseconds = req.params.selectedDate;
    var selectedDate = new Date(parseInt(milliseconds));

    const appointmentsQuery = Appointment.find({ startDate: { "$gte": selectedDate.setHours(0, 0, 0, 0), "$lt": selectedDate.setHours(23, 59, 59, 59) } });

    appointmentsQuery
        .then(documents => {
            res.status(200).json({
                message: 'Appointments fetched successfully',
                appointments: documents
            });
        })
        .catch(error => {
            res.status(500).json({
                message: 'A server error occurred while fetching appointments'
            });
        });
};

exports.getAppointmentsByWeek = (req, res, next) => {
    const milliseconds = req.params.selectedDate;
    const selectedDate = new Date(parseInt(milliseconds));
    const startOfWeek = new Date(moment(selectedDate).startOf('week'));
    const endOfWeek = new Date(moment(selectedDate).endOf('week'));

    const appointmentsQuery = Appointment.find({ startDate: { "$gte": startOfWeek, "$lt": endOfWeek } });

    appointmentsQuery
        .then(documents => {
            res.status(200).json({
                message: 'Appointments fetched successfully',
                appointments: documents
            });
        })
        .catch(error => {
            res.status(500).json({
                message: 'A server error occurred while fetching appointments'
            });
        });
};

exports.createAppointment = async(req, res, next) => {
    const patient = await User.findById(req.body.patient._id);
    if (!patient) {
        return res.status(404).json({ message: 'Patient not found' });
    }

    const appointment = new Appointment({
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        patient: {
            _id: patient._id,
            name: patient.name,
            phoneNumber: patient.phoneNumber
        },
        notes: req.body.notes,
        doctor: req.body.doctor,
        createdByUserId: req.body.createdByUserId
    });
    appointment.save()
        .then(createdAppointment => {
            res.status(201).json({
                message: 'Appointment created successfully',
                appointment: {
                    // ...createdAppointment,
                    // id: createdAppointment._id
                    id: createdAppointment._id,
                    startDate: createdAppointment.startDate,
                    endDate: createdAppointment.endDate,
                    patient: {
                        id: createdAppointment.patient._id,
                        name: createdAppointment.patient.name,
                        phoneNumber: createdAppointment.patient.phoneNumber
                    },
                    notes: createdAppointment.notes,
                    doctor: createdAppointment.doctor,
                    createdByUserId: createdAppointment.createdByUserId
                }
            });
        })
        .catch(error => {
            res.status(500).json({
                message: 'A server error occurred while creating appointment: ' + error
            })
        })
};

exports.updateAppointment = (req, res, next) => {
    const appointment = new Appointment({
        _id: req.body.id,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        patient: {
            _id: req.body.patient.id,
            name: req.body.patient.name,
            phoneNumber: req.body.patient.phoneNumber
        },
        notes: req.body.notes,
        doctor: req.body.doctor,
        createdByUserId: req.body.createdByUserId
    });
    Appointment.updateOne({ _id: req.params.id }, appointment)
        .then(result => {
            if (result.n > 0) {
                res.status(200).json({ message: 'Update successful!' });
            } else {
                res.status(401).json({ message: 'Not authorized' });
            }
        })
        .catch(error => {
            res.status(500).json({
                message: 'Could not update the user with id ' + _id + ': ' + error
            });
        });
}

exports.deleteAppointment = (req, res, next) => {
    Appointment.deleteOne({ _id: req.params.id })
        .then(result => {
            console.log(result);

            if (result.n > 0) {
                res.status(200).json({ message: 'Deletion of appointment is successful' });
            } else {
                res.status(400).json({ message: 'Not authorized to delete appointment' });
            }
        })
        .catch(error => {
            res.status(500).json({
                message: 'An error occurred while deleting appointmet'
            });
        });
};