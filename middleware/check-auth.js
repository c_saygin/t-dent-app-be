const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1]; // split is for extract 'Bearer ' from the token
        const decodedToken = jwt.verify(token, 'secret_private_key_make_it_env_variable'); //TODO: make the secret environment variable
        req.userData = {
            phoneNumber: decodedToken.phoneNumber,
            userId: decodedToken.userId,
            userType: decodedToken.userType
        };
        next();
    } catch (error) {
        res.status(401).json({ message: 'User is not authenticated' });
    }
};