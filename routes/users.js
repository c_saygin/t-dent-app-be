const express = require('express');

const userController = require('../controllers/users');
const chechAuthPatientOperations = require('../middleware/check-auth-patient-operations');
const checkAuthNurseOperations = require('../middleware/check-auth-nurse-operations');
const checkAuthDoctorOperations = require('../middleware/check-auth-doctor-operations');

const router = express.Router();

router.post('/create-user', chechAuthPatientOperations, userController.createUser);
router.post('/login', userController.loginUser);

//Patient Routes
router.get('/patients', chechAuthPatientOperations, userController.getPatients);
router.get('/patients/:phoneNumber', userController.getPatientByPhoneNumber)
router.put('/patients/:id', chechAuthPatientOperations, userController.updatePatient);
router.delete('/patients/:id', chechAuthPatientOperations, userController.deletePatient);
router.get('/user/:id', userController.getPatientById);

// Nurse Routes
router.get('/nurses', checkAuthNurseOperations, userController.getNurses);
router.get('/nurses/:phoneNumber', checkAuthNurseOperations, userController.getNurseByPhoneNumber)
router.put('/nurses/:id', checkAuthNurseOperations, userController.updateNurse);
router.delete('/nurses/:id', checkAuthNurseOperations, userController.deleteNurse);

// Doctor Routes
router.get('/doctors', checkAuthDoctorOperations, userController.getDoctors);
router.get('/doctors/:phoneNumber', checkAuthDoctorOperations, userController.getDoctorByPhoneNumber)
router.put('/doctors/:id', checkAuthDoctorOperations, userController.updateDoctor);
router.delete('/doctors/:id', checkAuthDoctorOperations, userController.deleteDoctor);

module.exports = router;