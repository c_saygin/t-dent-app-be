const express = require('express');

const appointmentController = require('../controllers/appointments');

const router = express.Router();

router.get('/:selectedDate', appointmentController.getAppointmentsByDate)
router.get('/week/:selectedDate', appointmentController.getAppointmentsByWeek)
router.post('/create', appointmentController.createAppointment);
router.put('/:id', appointmentController.updateAppointment);
router.delete('/:id', appointmentController.deleteAppointment);

module.exports = router;